<?php $title = 'Service'; include 'includes/__head.php';?>
<?php
  $serviceID = ' '; 
	$recentEventID = ' ';
	$AboutUsID = ' ';
  $ContactUsID = ' ';
  
  $Service = 'Service.php'; 
  $RecentEvents = 'gallery.php';
  $AboutUs = '#'; 
  $ContactUs = 'contactUs-page.php';
  
  include 'includes/views/nav.php';
  ?>
<?php include 'includes/views/slider.php';?>

<div class="individual-service-page-section">

  <?php
    $image = 'img/photo-3.jpg';
    $sarviceName = 'Other';
    $sarvice1 = 'Digital Print and Media';
    $sarvice2 = 'Team Building Activities';
    $sarvice3 = 'Hotel Reservation and Group Handling';
    $sarvice4 = 'Conference Planning';
    $sarvice5 = 'Visa Assistance';
    $sarvice6 = 'Logistic Supports';
    include 'includes/views/service-details.php';
  ?>
  <?php include 'includes/views/contact-icon.php';?>
  
</div>



<?php include 'includes/views/footer.php';?>
<?php include 'includes/__foot.php' ?>