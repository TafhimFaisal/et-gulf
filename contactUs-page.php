<?php $title = 'contact Us'; include 'includes/__head.php';?>
<?php
	$serviceID = ' '; 
	$recentEventID = ' ';
	$AboutUsID = ' ';
	$ContactUsID = ' ';

	$Service = 'Service.php'; 
	$RecentEvents = 'gallery.php';
	$AboutUs = '#'; 
	$ContactUs = 'contactUs-page.php';
  
  include 'includes/views/nav.php';
  ?>
<?php include 'includes/views/slider.php';?>
<!------ Include the above in your HEAD tag ---------->


	<section id="contact">
		
		<div class="contact-section">
			<div class="container">
				<form class="">
					<div class="row">

						<div class="col-md-6 form-line mt-5">
							<div class="form-group">
								
								<input type="text" class="form-control" id="" placeholder=" Enter Name">
							</div>
							<div class="form-group">
								
								<input type="email" class="form-control" id="exampleInputEmail" placeholder=" Enter Email id">
							</div>	
							<div class="form-group">
								
								<input type="tel" class="form-control" id="telephone" placeholder=" Enter 10-digit mobile no.">
							</div>
							<div class="form-group">
								
								<textarea  class="form-control" id="description" placeholder="Enter Your Message"></textarea>
							</div>
							<div>
								<button type="button" class="btn btn-default submit"><i class="fa fa-paper-plane" aria-hidden="true"></i>  Send Message</button>
							</div>
						</div>

						<div class="col-md-6 contact-info" style="pading:10px;">
							<div class="frame">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d462562.84786349937!2d54.94754911112761!3d25.07570732947029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f43496ad9c645%3A0xbde66e5084295162!2sDubai+-+United+Arab+Emirates!5e0!3m2!1sen!2sbd!4v1552424970047" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
							<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d776574.1281535246!2d54.37641426852026!3d25.289044278720468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f43496ad9c645%3A0xbde66e5084295162!2sDubai+-+United+Arab+Emirates!5e0!3m2!1sen!2sbd!4v1552424819937" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>	 -->
							<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2646.2531389455903!2d-89.23024619999998!3d48.4516724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4d5923dbfe697737%3A0xc9d925c9bc9573fa!2s116+Bruce+St%2C+Thunder+Bay%2C+ON+P7A+5W6!5e0!3m2!1sen!2sca!4v1424272264157" width="100%" height="100%" frameborder="0" ></iframe> -->
							</div>
						</div>
					</div>
				</form>
				<?php include 'includes/views/contact-icon.php';?>

			</div>
		</div>
		
	</section>
	<?php include 'includes/views/footer.php';?>

<?php include 'includes/__foot.php' ?>
