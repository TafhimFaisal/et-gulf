<section class="trusted-company">

    <div class="container-fluid">
        
        <div class="row mb-5 logo-container">

            <div class="col-12 py-4 ">

                <div class="row">
                    
                    <!--Breaking content-->
                    <div class="col-md-12">
                        <div class="breaking-box">
                            <div id="carouselbreaking" class="carousel slide" data-ride="carousel">
                                <!--breaking news-->
                                <div class="carousel-inner">
                                    <!--list post-->
                                    <span class="position-relative mx-2 badge rounded-0 title-of-logo trusted-compeny-title">Trusted by:</span> 
                                    <div class="carousel-item active">
                                        <div class="trusted-compeny-logo">
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l1.png" alt="" srcset=""> </a>
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l2.png" alt="" srcset=""> </a>
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l3.png" alt="" srcset=""> </a>
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l4.png" alt="" srcset=""> </a>
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l5.png" alt="" srcset=""> </a>
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l6.png" alt="" srcset=""> </a>
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l7.png" alt="" srcset=""> </a>
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l8.png" alt="" srcset=""> </a>
                                            
                                        </div>
                                    </div>
                                    
                                    <!--list post-->
                                    <div class="carousel-item">
                                    <div class="trusted-compeny-logo"></div>
                                        <div class="trusted-compeny-logo"> 
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l9.png" alt="" srcset=""> </a>                                             
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l10.png" alt="" srcset=""> </a>                                             
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l11.png" alt="" srcset=""> </a>                                             
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l12.png" alt="" srcset=""> </a>                                             
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l13.png" alt="" srcset=""> </a>                                             
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l14.png" alt="" srcset=""> </a>                                             
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l15.png" alt="" srcset=""> </a>                                             
                                            <a class="text-white" href="#"><img src="img/logo/truster-Compenny/l16.png" alt="" srcset=""> </a>                                             
                                                                                       
                                        </div>
                                    </div>
                                    
                                    <!--list post-->
                                    <!-- <div class="carousel-item">
                                        <a href="#"><span class="position-relative mx-2 badge rounded-0 title-of-logo">Trusted by:</span></a> <a class="text-white" href="#">Compeny Logo</a>
                                    </div> -->
                                    
                                    <!--list post-->
                                    <!-- <div class="carousel-item">
                                        <a href="#"><span class="position-relative mx-2 badge rounded-0 title-of-logo">Trusted by:</span></a> <a class="text-white" href="#">Compeny Logo</a>
                                    </div> -->
                                    
                                    <!--list post-->
                                    <!-- <div class="carousel-item">
                                        <a href="#"><span class="position-relative mx-2 badge rounded-0 title-of-logo">Trusted by:</span></a> <a class="text-white" href="#">Compeny Logoew </a>
                                    </div> -->
                                </div>
                                <!--end breaking news-->
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

</section>