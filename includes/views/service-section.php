
<section class="service-section" id="section-Service">
    <div class="container">

        <div class="row service">

            <div class="col-md-6 service-main">
                <img class="service-img" src="img/35.jpg" alt="" srcset="">

                <div class="mask-service">
                    <a href="events-management.php">Events management</a>
                </div>

            </div>

            <div class="col-md-6 service-scendary">

                <div class="row service-scendary-1">

                    <div class="col-md-12">
                        <img class="service-img" src="img/26.jpg" alt="" srcset="">
                        <div class="mask-service">
                            <a href="photography.php">
                                Photography
                            </a>

                        </div>
                    </div>

                </div>
            
                <div class="row service-scendary-2">

                    <div class="col-md-6">
                        <img class="service-img" src="img/3.jpeg" alt="" srcset="">

                        <div class="mask-service">
                            <a href="concert.php">
                                concert
                            </a>

                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <img class="service-img" src="img/2.jpg" alt="" srcset="">
                        <div class="mask-service">
                            <a href="other.php">
                                Other Service
                            </a>

                        </div>
                    </div>

                </div>

            </div>

        </div>


    </div>
</section>