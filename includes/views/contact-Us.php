
<section class="contact-section-1" id="section-ContactUs">

    <div class="container">

        <div class="con">

            <div class="box">
                <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                <div class='details'>
                    <h3>PO Box:88796,Dubai,UAE</h3>
                </div>
            </div>
            
            <div class="box">
                <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                <div class='details'><h3>+971-58-9403104</h3></div>
            </div>
            
            <div class="box">
                <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                <div class='details'><h3>info@et-gulf.com</h3></div>
            </div>

        </div>

    </div>


</section>