

<!-- Footer -->
	<section id="footer">
		<div class="container footer-link ">
			<div class="row text-center text-xs-center text-sm-left text-md-left d-flex justify-content-center">
				<div class="col-xs-12 col-sm-4 col-md-3">
					
					<ul class="list-unstyled quick-links">
						<li><a href="#">Other Pages</a></li>
						<li><a href="index.php"><i class="fa fa-angle-double-right"></i>Home</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>About Us</a></li>
						<li><a href="gallery.php"><i class="fa fa-angle-double-right"></i>Gallery</a></li>
						<li><a href="gallery.php"><i class="fa fa-angle-double-right"></i>Recant Event</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-3">
					
					<ul class="list-unstyled quick-links">
						<li><a href="#">Other Info</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Policy</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
						<li><a href="contactUs-page.php"><i class="fa fa-angle-double-right"></i>Contact Us</a></li>
						<li><a href="http://tafhimfaisal.tk/" target="_blank"><i class="fa fa-angle-double-right"></i>Devaloper</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-3">
										
					<ul class="list-unstyled quick-links">
						<li><a href="#">Other Services</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Team Building Activities</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Hotel Reservation </a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Group Handling</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Visa Assistance</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Logistic Supports</a></li>
					</ul>
				</div>
				<!-- <div class="col-xs-12 col-sm-4 col-md-3">
										
					<ul class="list-unstyled quick-links">
						<li><a href="#">Main Services</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Event planning</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>concert planning</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Photography</a></li>
						<li><a href="#"><i class="fa fa-angle-double-right"></i>Digital Print and Media</a></li>
					</ul>
				</div> -->
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
					<ul class="list-unstyled list-inline social text-center">
						<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li class="list-inline-item"><a href="#" target="_blank"><i class="fa fa-envelope"></i></a></li>
					</ul>
				</div>
				</hr>
			</div>	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p>This site is designed by <u><a href="http://tafhimfaisal.tk/" target="_blank">Tafhim</a></u></p>
					<p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="" target="_blank">et-gulf</a></p>
				</div>
				</hr>
			</div>	
		</div>
	</section>
	<!-- ./Footer -->
