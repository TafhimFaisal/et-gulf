
    <section id="contact-icon">

       <div class="container">
           <!-- contact -->
           <div class="row">
             
             <div class="col-sm-12 col-md-6 col-lg-4 my-5">
               <div class="card border-0">
                  <div class="card-body text-center">
                    <i class="fa fa-phone fa-5x mb-2" aria-hidden="true"></i>
                    <h4 class="text-uppercase">call us</h4>
                    <p class="text-uppercase">
                        +971-58-9403104
                    </p>
                    
                  </div>
                </div>
             </div>
             
             <div class="col-sm-12 col-md-6 col-lg-4 my-5">
               <div class="card border-0">
                  <div class="card-body text-center">
                    <i class="fa fa-map-marker fa-5x mb-2" aria-hidden="true"></i>
                    <h4 class="text-uppercase">office loaction</h4>
                   <address>PO Box: 88796, Dubai,UAE</address>
                  </div>
                </div>
             </div>
             
             <div class="col-sm-12 col-md-6 col-lg-4 my-5">
               <div class="card border-0">
                  <div class="card-body text-center">
                    <i class="fa fa-globe fa-5x mb-2" aria-hidden="true"></i>
                    <h4 class="text-uppercase">email</h4>
                    <p>info@et-gulf.com</p>
                  </div>
                </div>
             </div>
           </div>
           <!-- contact -->
           
           <div class="social-midia">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                <div style="clear: both;"></div>
              </ul>
            </div>
          </div>
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>
</html>