<!-- 
ml-auto
mr-auto  
-->
<div class="container-fluid">

  <nav class="fixed-top navbar navbar-expand-lg">

      <a class="navbar-brand" href="index.php">
        <img src="img/logo/Eventailor-Logo.png" alt="" srcset="">
      </a>

      <div class="container">
      
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse main-navbar" id="navbarTogglerDemo01">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0 main-navbar-ul">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="<?php echo $serviceID ?>" href= <?php echo $Service ?> >Service</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="<?php echo $recentEventID ?>" href= <?php echo $RecentEvents ?> >Gallery</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="<?php echo $AboutUsID ?>" href= <?php echo $AboutUs ?> >About Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="<?php echo $ContactUsID ?>" href= <?php echo $ContactUs ?> >Contact Us</a>
            </li>
          </ul>
        </div>
      </div>

  </nav>
</div>