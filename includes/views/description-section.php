
<section class="description-section" id="section-Description">

    <div class="container-fluid">
    
        <div class="row">
            <div class="col-md-4 description-section-image">
                <img src="img/42.jpg" alt="" srcset="">                
            </div>
            <div class="col-md-8 description-section-dec">
                <p style="font-size:25px">
                    Eventailor LLC is a UAE based Event Management 
                    Company, executing Events throughout the GULF. 
                    Eventailor LLC was established on July 2nd, 2008 
                    and with the combined effort of Sales and Operations 
                    Team, Eventailor LLC has been growing up with a prestigious 
                    image of Customer Handling. From large scale consumer 
                    festivals and community celebrations to private corporate 
                    events and world class entertainment venues, we have a 
                    passion for creating memorable event experiences.
                    <br/>
                    <br/>
                    We are in Service Oriented Industry, 
                    so Client’s satisfaction is the only 
                    key to proceed ahead. Keeping it in 
                    mind, Eventailor Team is dedicated to 
                    execute events as per the client’s needs 
                    perfectly. 
                </p>

                
            </div>
        </div>
    </div>

</section>