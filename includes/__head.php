<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

 
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/nav.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/recentEvent.js"></script>
    <script src="js/recentEvent-section.js"></script> 
    <script src="js/contact-Us.js"></script>
    <script src="js/gallery.js"></script>
  
    <!-- gellary -->
    <link rel="stylesheet" href="stylesheets/magnific-popup.css">
    <script src="js/head-js.js"></script>
    <script src="js/isotope.pkgd.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- gellary -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
    <link rel="stylesheet" href="stylesheets/all.css">
    <link rel="stylesheet" href="stylesheets/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Oleo+Script:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Teko:400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- contact-icon -->
    <link rel="stylesheet" href="stylesheets/contact-icon.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      

    <title><?php echo $title ?></title>
  </head>
  
  <body>