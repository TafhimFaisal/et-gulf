<?php $title = 'Home'; include 'includes/__head.php';?>
<?php

    $serviceID = 'service'; 
    $recentEventID = 'recentEvent';
    $AboutUsID = 'About-Us';
    $ContactUsID = 'Contact-Us';

  $Service = '#section-Service'; 
  $RecentEvents = '#section-recentEvent';
  $AboutUs = '#section-Description'; 
  $ContactUs = '#section-ContactUs';

  include 'includes/views/nav.php';
  ?>
<?php include 'includes/views/slider.php';?>
<?php include 'includes/views/service-section.php';?>
<?php include 'includes/views/recentEvent-section.php';?>
<?php include 'includes/views/description-section.php';?>
<?php include 'includes/views/trusted-company.php';?>
<?php include 'includes/views/contact-Us.php';?>
<?php include 'includes/views/footer.php';?>
    
    
<?php include 'includes/__foot.php' ?>
