
$(window).scroll(function(){
    var wScroll = $(this).scrollTop();
    // console.log(wScroll);
    if(wScroll>50){
        $('nav').css({
            'background-color': 'black',
            'height': '50px',
            // 'font-size': '5px',
        })
        $('nav li').css({
            // 'font-size': '5px',
            //'margin-right': '30px'
        })
        $('nav li:nth-child(4').css({
            // 'font-size': '5px',
            'margin-right': ''
        })

        $('nav li a').css({
            'font-size': '17px',    
        })
        $('.navbar-brand').css({
            'margin-top': '1px',    
        })
        $('.navbar-brand img').css({
            'height': '35%',    
            'width': '35%',    
        })
        
    }else{
        $('nav').css({
            'background-color': '',
            'height': ''
        })
        
        $('nav li').css({
            'margin-right': ''
        })
        $('nav li a').css({
            'font-size': '',    
        })
        
        $('.navbar-brand').css({
            'margin-top': '',    
        })
        $('.navbar-brand img').css({
            'height': '',    
            'width': '',    
        })
    }
})

$(document).ready(function(){
    $("#service").click(function() {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $("#section-Service").offset().top
        }, 1000);
    });
    $("#About-Us").click(function() {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $("#section-Description").offset().top
        }, 1000);
    });
    $("#Contact-Us").click(function() {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $("#section-ContactUs").offset().top
        }, 1000);
    });
    $("#recentEvent").click(function() {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $("#section-recentEvent").offset().top
        }, 1000);
    });
})