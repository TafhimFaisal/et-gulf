<?php $title = 'Service'; include 'includes/__head.php';?>
<?php
  $serviceID = ' '; 
	$recentEventID = ' ';
	$AboutUsID = ' ';
  $ContactUsID = ' ';
  
  $Service = 'Service.php'; 
  $RecentEvents = 'gallery.php';
  $AboutUs = '#'; 
  $ContactUs = 'contactUs-page.php';
  
  include 'includes/views/nav.php';
  ?>
<?php include 'includes/views/slider.php';?>

<div class="individual-service-page-section">

  <?php
    $image = 'img/photo-3.jpg';
    $sarviceName = 'Event planning';
    $sarvice1 = 'Exhibition';
    $sarvice2 = 'Seminar';
    $sarvice3 = 'Conference Planning';
    $sarvice4 = 'Product Launch';
    $sarvice5 = 'Branding';
    $sarvice6 = 'Fashion Show';
    include 'includes/views/service-details.php';
  ?>
  <?php include 'includes/views/contact-icon.php';?>
  
</div>



<?php include 'includes/views/footer.php';?>
<?php include 'includes/__foot.php' ?>