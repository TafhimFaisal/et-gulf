
<?php $title = 'Home'; include 'includes/__head.php';?>
<?php
   $serviceID = ' '; 
   $recentEventID = ' ';
   $AboutUsID = ' ';
   $ContactUsID = ' ';

  $Service = 'Service.php'; 
  $RecentEvents = 'gallery.php';
  $AboutUs = '#'; 
  $ContactUs = 'contactUs-page.php';
  
  include 'includes/views/nav.php';
  ?>
<?php include 'includes/views/slider.php';?>
      
   <section class="container-fluid gellary-section">
   
   <div class="container">

            <div class="portfolio-menu mt-2 mb-4">
               <ul>
                  <li class="btn btn-outline-dark" data-filter="*">All</li>
                  <li class="btn btn-outline-dark " data-filter=".gts">Girls T-shirt</li>
                  <li class="btn btn-outline-dark " data-filter=".lap">Laptops</li>
                  <li class="btn btn-outline-dark text active" data-filter=".selfie">selfie</li>
               </ul>
            </div>


            <div class="portfolio-item row" style="position: relative; height: 731.655px;">
               
               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 0px;">
                  <a href="img/image-gallery/stylish-young-woman-with-bags-taking-selfie_23-2147962203.jpg" class="fancylight popup-btn" data-fancybox-group="light"> 
                  <img class="img-fluid" src="img/image-gallery/stylish-young-woman-with-bags-taking-selfie_23-2147962203.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 0px; display: none;">
                  <a href="img/image-gallery/pretty-girl-near-car_1157-16962.jpg" class="fancylight popup-btn" data-fancybox-group="light"> 
                  <img class="img-fluid" src="img/image-gallery/pretty-girl-near-car_1157-16962.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 0px;">
                  <a href="img/image-gallery/blonde-tourist-taking-selfie_23-2147978899.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/blonde-tourist-taking-selfie_23-2147978899.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 0px; display: none;">
                  <a href="img/image-gallery/cute-girls-oin-studio_1157-18211.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/cute-girls-oin-studio_1157-18211.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 0px; display: none;">
                  <a href="img/image-gallery/stylish-pin-up-girls_1157-18451.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/stylish-pin-up-girls_1157-18451.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 0px; display: none;">
                  <a href="img/image-gallery/stylish-pin-up-girl_1157-18940.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/stylish-pin-up-girl_1157-18940.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 0px; display: none;">
                  <a href="img/image-gallery/digital-laptop-working-global-business-concept_53876-23438.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/digital-laptop-working-global-business-concept_53876-23438.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 0px; display: none;">
                  <a href="img/image/set-digital-devices-screen-mockup_53876-76507.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/set-digital-devices-screen-mockup_53876-76507.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/young-brunette-woman-with-sunglasses-urban-background_1139-893.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/young-brunette-woman-with-sunglasses-urban-background_1139-893.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 0px; display: none;">
                  <a href="img/image-gallery/laptop-digital-device-screen-mockup_53876-76509.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/laptop-digital-device-screen-mockup_53876-76509.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/young-woman-holding-pen-hand-thinking-while-writing-notebook_23-2148029424.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/young-woman-holding-pen-hand-thinking-while-writing-notebook_23-2148029424.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/female-fashion-concept_23-2147643598.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/female-fashion-concept_23-2147643598.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/girl-city_1157-16454.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/girl-city_1157-16454.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 359.718px; display: none;">
                  <a href="img/image-gallery/elegant-lady-with-laptop_1157-16643.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/elegant-lady-with-laptop_1157-16643.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 0px; display: none;">
                  <a href="img/image-gallery/laptop-mock-up-lateral-view_1310-199.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/laptop-mock-up-lateral-view_1310-199.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 359.718px; display: none;">
                  <a href="img/image-gallery/portrait-young-woman_1303-10071.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/portrait-young-woman_1303-10071.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 359.718px; display: none;">
                  <a href="img/image-gallery/beautiful-girl-near-wall_1157-16401.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/beautiful-girl-near-wall_1157-16401.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 0px;">
                  <a href="img/image-gallery/woman-taking-photograph-her-boyfriend-enjoying-piggyback-ride-his-back_23-2147841613.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/woman-taking-photograph-her-boyfriend-enjoying-piggyback-ride-his-back_23-2147841613.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 0px;">
                  <a href="img/image-gallery/girl-smiling-making-auto-photo-with-her-friends-around_1139-593.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/girl-smiling-making-auto-photo-with-her-friends-around_1139-593.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 179.859px;">
                  <a href="img/image-gallery/multiracial-group-young-people-taking-selfie_1139-1032.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/multiracial-group-young-people-taking-selfie_1139-1032.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/laptop-wooden-table_53876-20635.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/laptop-wooden-table_53876-20635.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/business-woman-working-laptop_1388-67.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/business-woman-working-laptop_1388-67.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/group-people-holding-laptop-mockup-charity_23-2148069565.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/group-people-holding-laptop-mockup-charity_23-2148069565.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 359.718px; display: none;">
                  <a href="img/image-gallery/portrait-young-cheerful-woman-headphones-sitting-stairs_1262-17488.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/portrait-young-cheerful-woman-headphones-sitting-stairs_1262-17488.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 539.577px; display: none;">
                  <a href="img/image-gallery/celebration-concept-close-up-portrait-happy-young-beautiful-african-woman-black-t-shirt-smiling-with-colorful-party-balloon-yellow-pastel-studio-background_1258-934.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/celebration-concept-close-up-portrait-happy-young-beautiful-african-woman-black-t-shirt-smiling-with-colorful-party-balloon-yellow-pastel-studio-background_1258-934.jpg" alt="">
                  </a>
               </div>

               <div class="item gts col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 539.577px; display: none;">
                  <a href="img/image-gallery/pretty-woman-showing-arm-muscles_23-2148056021.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/pretty-woman-showing-arm-muscles_23-2148056021.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 179.859px; display: none;">
                  <a href="img/image-gallery/blank-colorful-adhesive-notes-against-wooden-wall-with-office-stationeries-laptop_23-2148052717.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/blank-colorful-adhesive-notes-against-wooden-wall-with-office-stationeries-laptop_23-2148052717.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 361.75px; display: none;">
                  <a href="img/image-gallery/happy-woman-having-video-call-using-laptop-office_23-2148056211.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/happy-woman-having-video-call-using-laptop-office_23-2148056211.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 361.75px; display: none;">
                  <a href="img/image-gallery/laptop-mockup-table-with-plants_23-2147955548.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/laptop-mockup-table-with-plants_23-2147955548.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 361.75px; display: none;">
                  <a href="img/image-gallery/blank-colorful-adhesive-notes-against-wooden-wall-with-office-stationeries-laptop_23-2148052717.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/blank-colorful-adhesive-notes-against-wooden-wall-with-office-stationeries-laptop_23-2148052717.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 361.75px; display: none;">
                  <a href="img/image-gallery/woman-using-laptop-smartphone_53876-76350.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/woman-using-laptop-smartphone_53876-76350.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 179.859px;">
                  <a href="img/image-gallery/attractive-young-woman-with-curly-hair-takes-selfie-posing-looking-camera_8353-6636.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/attractive-young-woman-with-curly-hair-takes-selfie-posing-looking-camera_8353-6636.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 179.859px;">
                  <a href="img/image-gallery/young-couple-taking-selfie-mobile-phone-against-blue-background_23-2148056292.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/young-couple-taking-selfie-mobile-phone-against-blue-background_23-2148056292.jpg" alt="">
                  </a>
               </div>

               <div class="item lap col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 553.828px; display: none;">
                  <a href="img/image-gallery/close-up-blonde-woman-sitting-sofa-using-laptop-with-blank-white-screen_23-2148028738.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/close-up-blonde-woman-sitting-sofa-using-laptop-with-blank-white-screen_23-2148028738.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 855px; top: 179.859px;">
                  <a href="img/image-gallery/group-happy-friends-taking-selfie-cellphone_23-2147859575.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/group-happy-friends-taking-selfie-cellphone_23-2147859575.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 371.937px;">
                  <a href="img/image-gallery/joyful-pretty-girl-with-curly-hair-takes-selfie-mobile-phone_8353-6635.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/joyful-pretty-girl-with-curly-hair-takes-selfie-mobile-phone_8353-6635.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 371.937px;">
                  <a href="img/image-gallery/attractive-young-woman-with-curly-hair-takes-selfie-posing-looking-camera_8353-6636.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/attractive-young-woman-with-curly-hair-takes-selfie-posing-looking-camera_8353-6636.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 570px; top: 371.937px;">
                  <a href="img/image-gallery/multiracial-group-young-people-taking-selfie_1139-1032.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/multiracial-group-young-people-taking-selfie_1139-1032.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-6 col-sm" style="position: absolute; left: 855px; top: 371.937px;">
                  <a href="img/image-gallery/two-smiling-girls-take-selfie-their-phones-posing-with-lollipops_8353-5600.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/two-smiling-girls-take-selfie-their-phones-posing-with-lollipops_8353-5600.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 0px; top: 551.796px;">
                  <a href="img/image-gallery/female-friends-sitting-car-hood-taking-self-portrait_23-2147855623.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/female-friends-sitting-car-hood-taking-self-portrait_23-2147855623.jpg" alt="">
                  </a>
               </div>

               <div class="item selfie col-lg-3 col-md-4 col-6 col-sm" style="position: absolute; left: 285px; top: 551.796px;">
                  <a href="img/image-gallery/two-smiling-girls-take-selfie-their-phones-posing-with-lollipops_8353-5600.jpg" class="fancylight popup-btn" data-fancybox-group="light">
                  <img class="img-fluid" src="img/image-gallery/two-smiling-girls-take-selfie-their-phones-posing-with-lollipops_8353-5600.jpg" alt="">
                  </a>
               </div>

            </div>
            
         </div>
         </section>
<?php include 'includes/views/footer.php';?>
<script src="js/foot-js.js"></script>


  